# from django.http import HttpResponse
from django.shortcuts import render
from djangoProject.models import Ticket


def index(request):
    # return HttpResponse("Hello my day!")
    return render(request, 'index.html')

def submit(request):
    new_ticket = Ticket(submitter="Test User", body="new life new way new user")
    new_ticket.save()
    return render(request, 'submit.html')

def tickets(request):
    return render(request, 'tickets.html')